import os
import os.path
import shutil
import stat

# The directory where cmake wrote its build products.
# This directory tree is what goes into the rez package.
CMAKE_BUILD_SUBDIR = 'build'


def build(source_path, build_path, install_path, targets):

    cmake_build_path = os.path.join(source_path, CMAKE_BUILD_SUBDIR)

    folders = [x for x in next(os.walk(cmake_build_path))[1] if not x.startswith(".")]

    def _build():
        for name in folders:
            src = os.path.join(cmake_build_path, name)
            dest = os.path.join(build_path, name)

            if os.path.exists(dest):
                shutil.rmtree(dest)
                
            shutil.copytree(src, dest)

    def _install():
        for name in folders:
            src = os.path.join(build_path, name)
            dest = os.path.join(install_path, name)

            if os.path.exists(dest):
                try:
                    shutil.rmtree(dest)
                except WindowsError:
                    pass
                    
            print src
            print dest
            shutil.copytree(src, dest)

    _build()

    if "install" in (targets or []):
        _install()
