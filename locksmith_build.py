import os
import sys
import shutil
import subprocess

al_version = "0.31.1"
usd_version = "19.05"

path = os.path.join(r"S:\packages\int\AL_USD", al_version)
install_dir = path
usd_location = os.path.join(r"S:\packages\int\USD", usd_version)
if "-local" in sys.argv:
    path = os.path.dirname(os.path.realpath(__file__))
    install_dir = os.path.join(path, "install")
    usd_location = os.path.join(os.path.dirname(path), "usd", "build")

build_dir = os.path.join(path, "build")
maya_path = "C:\\Program Files\\Autodesk\\Maya2018"

if "-build" in sys.argv:
    if not os.path.exists(build_dir):
        os.makedirs(build_dir)
    if not os.path.exists(install_dir) and "-local" in sys.argv:
        os.makedirs(install_dir)

    if os.listdir(build_dir) and os.listdir(install_dir):
        print "Already contains a non-empty 'Build' or 'Install' folder, please delete manually and re-run."
    else:
        # Go to the installed USD\build\path\cmake\pxrTargets-release.cmake and comment the following lines:
        #  First create a wrapper script called setup_environment.bat:
        env_bat_file_path = "setup_build_environment_temp.bat"
        env_txt_file_path = "build_environment_temp.txt"
        exists = os.path.isfile(os.path.join(os.path.expandvars('%VS140COMNTOOLS%'), "VsDevCmd.bat"))
        if exists:
            with open(env_bat_file_path, "w") as env_bat_file:
                env_bat_file.write('call "%VS140COMNTOOLS%VsDevCmd.bat"\n')
                env_bat_file.write("set > %s\n" % env_txt_file_path)

            os.system(env_bat_file_path)
            with open(env_txt_file_path, "r") as env_txt_file:
                lines = env_txt_file.read().splitlines()

            # run the wrapper script, then read the environment variables from the text file into your current environment
            os.remove(env_bat_file_path)
            os.remove(env_txt_file_path)
            for line in lines:
                pair = line.split("=", 1)
                os.environ[pair[0]] = pair[1]

            os.environ['Path'] += ";" + r"C:\Program Files\Autodesk\Maya2018\bin" + ";"
            os.environ['Path'] += r"S:\tools\CMake\bin"

            build_cmd = 'cmake ' \
                  '-G "Visual Studio 14 2015 Win64" ' \
                  '-DCMAKE_INSTALL_PREFIX="{plugin_install_location}" ' \
                  '-DCMAKE_BUILD_TYPE=Release ' \
                  '-DBOOST_ROOT="{boost_src}" ' \
                  '-DBOOST_LIBRARYDIR="{boost_lib}" ' \
                  '-DMAYA_LOCATION="{maya_location}" ' \
                  '-DUSD_ROOT="{usd_location}" ' \
                  '-DSKIP_USDMAYA_TESTS=ON ' \
                  '-DBUILD_USDMAYA_SCHEMAS=OFF "{source_files}"'. \
                format(plugin_install_location=install_dir,
                        boost_src=os.path.join(usd_location, "src"),
                        boost_lib=os.path.join(usd_location, "lib"),
                        maya_location=maya_path,
                        usd_location=usd_location,
                        source_files=os.path.dirname(os.path.realpath(__file__)))

            os.chdir(build_dir)
            subprocess.call(build_cmd, env=os.environ)

            install_cmd = 'cmake --build . --target install --config Release -- /M:16'
            subprocess.call(install_cmd, env=os.environ)

            print "USD installed to 'build' folder."

        else:
            print "file: C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\vcvarsall.bat doesn't exists on this machine. Please install Microsoft Visual studio."

if "-release" in sys.argv and "-local" in sys.argv:
    if not os.path.exists(install_dir):
        if os.listdir(install_dir):
            print "'Install' folder doesn't exists or is empty. Run with -build flag."
    else:
        # check if version exists on S:
        install_dir = r"S:\packages\int\AL_USD"
        if not os.path.exists(install_dir):
            os.makedirs(install_dir)
        install_path = os.path.join(install_dir, al_version)
        if not os.path.exists(install_path):
            os.makedirs(install_path)

        try:
            shutil.rmtree(install_path)
        except WindowsError:
            pass

        shutil.copytree(install_dir, install_path)

        print "AL_USD installed at %s" % (install_path)