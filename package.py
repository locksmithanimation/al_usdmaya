name = "al_usdmaya"

version = "0.31.1"

authors = [
    "Deepak Mathur"
]

description = \
    """
    Animal Logic USD Maya Plug-ins
    """

tools = []

requires = ['usd']

uuid = "locksmith_animal_logic_usd_maya_lib"

def commands():
    env.MAYA_PLUG_IN_PATH.append('{this.root}/plugin')
    env.MAYA_SCRIPT_PATH.append('{this.root}/lib/usd/AL_USDMaya/resources')
    env.PXR_PLUGINPATH_NAME.append('{this.root}/lib/usd')
    env.PYTHONPATH.append('{this.root}/lib/python')
    env.PATH.append('{this.root}/lib')
